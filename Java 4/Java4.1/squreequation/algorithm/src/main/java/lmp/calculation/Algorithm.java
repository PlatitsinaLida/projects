//Создайте отдельный maven-проект. В нем — класс с полем типа «Квадратный трехчлен»
// из предыдущего проекта. Класс содержит метод, который возвращает больший корень
// или выбрасывает исключение, если корней нет.
// Напишите для него тесты.

package lmp.calculation;


import java.util.Arrays;

public class Algorithm {
    public static double theGreatestRoot(SquareTrinomial s) {
        double great;
        Arrays.sort(s.quadraticSolution());
        great = s.quadraticSolution()[1];
        return great;
    }

    /* Филиппов А.В. 10.06.2020 Комментарий не удалять.
     Странный junit тест
    */
    public static void main(String[] args) {
        SquareTrinomial s1 = new SquareTrinomial(3, 6, 3);
        System.out.println(theGreatestRoot(s1));
    }
}
