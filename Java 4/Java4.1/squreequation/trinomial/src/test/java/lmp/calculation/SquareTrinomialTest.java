package lmp.calculation;


/**
 * Unit test for simple App.
 */
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SquareTrinomialTest {

    @Test
    public void quadraticSolution() {
       SquareTrinomial squareTrinomial = new SquareTrinomial(1,2,-3);
       double[] roots = new double[2];
        roots[0] = -3;
        roots[1] = 1;
        Arrays.sort(roots);
        Arrays.sort(squareTrinomial.quadraticSolution());
        assertArrayEquals(roots, squareTrinomial.quadraticSolution());
    }
    @Test
    public void quadraticSolution1()
    {
        SquareTrinomial s= new SquareTrinomial(2,6,9);
        assertThrows(IllegalArgumentException.class,()->s.quadraticSolution());
    }

    @Test
    public void quadraticSolution2()
    {
        SquareTrinomial s1 =new SquareTrinomial(3,6,3);
        double[] roots = new double[2];
        roots[0] = -1;
        roots[1] = -1;
        Arrays.sort(roots);
        Arrays.sort(s1.quadraticSolution());
        assertArrayEquals(roots, s1.quadraticSolution(),1e-9);

    }

}