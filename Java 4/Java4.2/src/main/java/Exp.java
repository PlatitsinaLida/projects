//Написать набор классов, реализующих этот интерфейс функции вида f(x) = Aexp(x) + B.

public class Exp implements FunctionOnTheSegment {
    private double A;
    private double B;
    private double intervalA;
    private double intervalB;

    public Exp(double a, double b, double interA, double interB) {
        if (interA > interB) {
            throw new IllegalArgumentException("incorrect interval");
        }
        this.intervalA = interA;
        this.intervalB = interB;
        this.A = a;
        this.B = b;
    }

    public double functionValue(double x) {
        if (x >= intervalA && x <= intervalB) {
            return A * Math.exp(x) + B;
        } else {
            throw new IllegalArgumentException("incorrect x");
        }
    }

    public double getStart() {
        return intervalA;
    }

    public double getFinish() {
        return intervalB;
    }

    public double getMiddle() {
        return (intervalA + intervalB) / 2;
    }
}
