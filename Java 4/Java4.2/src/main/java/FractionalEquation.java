//Написать набор классов, реализующих этот интерфейс функции вида f(x) = (Ax + B) / (Cx + D).
public class FractionalEquation implements FunctionOnTheSegment {
    private double A;
    private double B;
    private double C;
    private double D;
    private double intervalA;
    private double intervalB;

    public FractionalEquation(double a, double b, double c, double d, double interA, double interB) {
        if (interA > interB) {
            throw new IllegalArgumentException("incorrect interval");
        }
        this.intervalA = interA;
        this.intervalB = interB;
        this.A = a;
        this.B = b;
        this.C = c;
        this.D = d;
    }

    public double functionValue(double x) {
        if (x >= intervalA && x <= intervalB) {
            return (A * x + B) / (C * x + D);
        } else {
            throw new IllegalArgumentException("incorrect x");
        }
    }

    public double getStart() {
        return intervalA;
    }

    public double getFinish() {
        return intervalB;
    }

    public double getMiddle() {
        return (intervalA + intervalB) / 2;
    }
}
