//Разработать интерфейс для понятия «функция одного вещественного аргумента,
// определенная на отрезке [a; b]».
// Интерфейс должен содержать метод вычисления значения функции при заданном аргументе
// и методы получения границ отрезка.
public interface FunctionOnTheSegment {
    double functionValue(double x);

    double getStart();

    double getFinish();

    double getMiddle();
}
