//определенный интеграл на отрезке [a; b] (пределы интегрирования хранятся как поля
// и устанавливаются конструктором, если область определения функции не содержится в [a; b],
// то выбрасывается исключение), интегрирование производить методом прямоугольников.
//f(x) = Asin(Bx).

/* Филиппов А.В. 10.06.2020 Комментарий не удалять.
 Не работает! Что помешало вам сделать один интеграл и одну сумму вместо 4 и 4?
 Если мы добавим еще с десяток функций, то вы добавите еще 10 интегралов?

 Вместо Sin в параметре должно стоять FunctionOnTheSegment
 и класс интеграл должен быть один!

 С суммой тоже самое.
*/

public class Integral implements FunctionalityFromASingleArgument<FunctionOnTheSegment> {

    @Override
    public double functionality(FunctionOnTheSegment one) {
        double integral = 0;
        int n = 100;
        double h = (one.getFinish() - one.getStart()) / n;
        for (int i = 0; i < n - 1; i++) {
            integral += h * one.functionValue(one.getStart() + i * h);
        }
        return integral;
    }
}
