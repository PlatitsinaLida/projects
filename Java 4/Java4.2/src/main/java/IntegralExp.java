//определенный интеграл на отрезке [a; b] (пределы интегрирования хранятся как
// поля и устанавливаются конструктором, если область определения функции не содержится
// в [a; b], то выбрасывается исключение), интегрирование производить методом прямоугольников.
//f(x) = Aexp(x) + B.
public class IntegralExp implements FunctionalityFromASingleArgument<Exp> {
    private Exp exp;

    @Override
    public double functionality(Exp one) {
        double integral = 0;
        int n = 100;
        double h = (one.getFinish() - one.getStart()) / n;
        for (int i = 0; i < n - 1; i++) {
            integral += h * one.functionValue(one.getStart() + i * h);
        }
        return integral;


    }
}
