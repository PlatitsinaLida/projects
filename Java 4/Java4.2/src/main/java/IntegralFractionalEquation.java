//определенный интеграл на отрезке [a; b] (пределы интегрирования хранятся как поля и
// устанавливаются конструктором, если область определения функции не содержится в [a; b],
// то выбрасывается исключение), интегрирование производить методом прямоугольников.
//f(x) = (Ax + B) / (Cx + D)
public class IntegralFractionalEquation implements FunctionalityFromASingleArgument<FractionalEquation> {
    public double functionality(FractionalEquation fractionalEquation) {
        double integral = 0;
        int n = 100;
        double h = (fractionalEquation.getFinish() - fractionalEquation.getStart()) / n;
        for (int i = 0; i < n - 1; i++) {
            integral += h * fractionalEquation.functionValue(fractionalEquation.getStart() + i * h);
        }
        return integral;
    }
}
