//определенный интеграл на отрезке [a; b] (пределы интегрирования хранятся как поля и
// устанавливаются конструктором, если область определения функции не содержится в [a; b],
// то выбрасывается исключение), интегрирование производить методом прямоугольников.
//f(x) = Ax + B.
public class IntegralLinearFunction implements FunctionalityFromASingleArgument<LinearFunction> {
    //private LinearFunction linearFunction;

    public double functionality(LinearFunction linearFunction) {
        double integral = 0;
        int n = 100;
        double h = (linearFunction.getFinish() - linearFunction.getStart()) / n;
        for (int i = 0; i < n - 1; i++) {
            integral += h * linearFunction.functionValue(linearFunction.getStart() + i * h);
        }
        return integral;
    }
}
