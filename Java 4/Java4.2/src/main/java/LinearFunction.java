//Написать набор классов, реализующих этот интерфейс функции вида f(x) = Ax + B.
public class LinearFunction implements FunctionOnTheSegment {

    private double A;
    private double B;
    private double intervalA;
    private double intervalB;

    public LinearFunction(double a, double b, double interA, double interB) {
        if (interA > interB) {
            throw new IllegalArgumentException("incorrect interval");
        }
        this.intervalA = interA;
        this.intervalB = interB;
        this.A = a;
        this.B = b;
    }

    /* Филиппов А.В. 10.06.2020 Комментарий не удалять.
     Обычно, когда есть предусловие (в нашем случае проверка границ отрезка), то сначала проверяют его,
     т.е. функция выглядит так

        if (x < intervalA || x > intervalB) {
            throw new IllegalArgumentException("incorrect x");
        }

        return A * x + B;
    */
    public double functionValue(double x) {
        if (x >= intervalA && x <= intervalB) {
            return A * x + B;
        } else {
            throw new IllegalArgumentException("incorrect x");
        }
    }

    public double getStart() {
        return intervalA;
    }

    public double getFinish() {
        return intervalB;
    }

    public double getMiddle() {
        return (intervalA + intervalB) / 2;
    }

}
