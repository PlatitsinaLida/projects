//Написать набор классов, реализующих этот интерфейс функции вида f(x) = Asin(Bx).
public class Sin implements FunctionOnTheSegment {

    private double A;
    private double B;
    private double intervalA;
    private double intervalB;

    public Sin(double a, double b, double interA, double interB) {
        if (interA > interB) {
            throw new IllegalArgumentException("incorrect interval");
        }
        this.intervalA = interA;
        this.intervalB = interB;
        this.A = a;
        this.B = b;
    }


    public double functionValue(double x) {
        if (x >= intervalA && x <= intervalB) {
            return A * Math.sin(B * x);
        } else {
            throw new IllegalArgumentException("incorrect x");
        }
    }

    public double getStart() {
        return intervalA;
    }

    public double getFinish() {
        return intervalB;
    }

    public double getMiddle() {
        return (intervalA + intervalB) / 2;
    }
}
