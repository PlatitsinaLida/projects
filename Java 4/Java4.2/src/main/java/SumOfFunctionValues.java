//сумма значений функции на концах отрезка и в его середине
//f(x) = Asin(Bx)

public class SumOfFunctionValues implements FunctionalityFromASingleArgument<FunctionOnTheSegment> {

    @Override
    public double functionality(FunctionOnTheSegment one) {
        double sum;
        sum = one.functionValue(one.getStart());
        sum += one.functionValue(one.getFinish());
        sum += one.functionValue(one.getMiddle());
        return sum;
    }
}
