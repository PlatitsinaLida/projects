//сумма значений функции на концах отрезка и в его середине
//f(x) = Aexp(x) + B
public class SumOfFunctionValuesExp implements FunctionalityFromASingleArgument<Exp> {

    public double functionality(Exp exp) {
        double sum;
        sum = exp.functionValue(exp.getStart());
        sum += exp.functionValue(exp.getMiddle());
        sum += exp.functionValue(exp.getFinish());
        return sum;
    }
}
