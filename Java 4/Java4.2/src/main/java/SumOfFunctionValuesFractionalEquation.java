//сумма значений функции на концах отрезка и в его середине
// f(x) = (Ax + B) / (Cx + D)
public class SumOfFunctionValuesFractionalEquation implements FunctionalityFromASingleArgument
        <FractionalEquation> {
    private FractionalEquation fractionalEquation;


    @Override
    public double functionality(FractionalEquation fractionalEquation) {
        double sum;
        sum = fractionalEquation.functionValue(fractionalEquation.getStart());
        sum += fractionalEquation.functionValue(fractionalEquation.getMiddle());
        sum += fractionalEquation.functionValue(fractionalEquation.getFinish());
        return sum;
    }
}
