//сумма значений функции на концах отрезка и в его середине
//f(x) = Ax + B

public class SumOfFunctionValuesLinearFunction implements FunctionalityFromASingleArgument<LinearFunction> {

    private LinearFunction linearFunction;


    @Override
    public double functionality(LinearFunction linearFunction) {
        double sum;
        sum = linearFunction.functionValue(linearFunction.getStart());
        sum += linearFunction.functionValue(linearFunction.getMiddle());
        sum += linearFunction.functionValue(linearFunction.getFinish());
        return sum;
    }
}
