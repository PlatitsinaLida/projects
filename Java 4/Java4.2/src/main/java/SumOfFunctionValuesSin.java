//сумма значений функции на концах отрезка и в его середине
//f(x) = Asin(Bx)

public class SumOfFunctionValuesSin implements FunctionalityFromASingleArgument<Sin> {
    private Sin sin;

    @Override
    public double functionality(Sin sin) {
        double sum;
        sum = sin.functionValue(sin.getStart());
        sum += sin.functionValue(sin.getFinish());
        sum += sin.functionValue(sin.getMiddle());
        return sum;
    }
}
