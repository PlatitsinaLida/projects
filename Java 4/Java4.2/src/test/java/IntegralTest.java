import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

public class IntegralTest {


    @Test
    public void functionalitySinIntegral() {
        Integral integralSin = new Integral();
        Sin sin = new Sin(1, 0, 0.0, 2.0);
        assertEquals(0.0, integralSin.functionality(sin), 1e-9);
        Sin sin1 = new Sin(0, -1, 0.0, 1.0);
        assertEquals(0.0, integralSin.functionality(sin1), 1e-9);

    }

    @Test
    public void functionalityFractionIntegral() {
        Integral integralFractionalEquation = new Integral();
        FractionalEquation fractionalEquation = new FractionalEquation(2, 0, 0, 1, 0, 0);
        FractionalEquation fractionalEquation1 = new FractionalEquation(2, 0, 0, 1, 1, 2);
        assertEquals(2.9502000000000006, integralFractionalEquation.functionality(fractionalEquation1), 1e-9);
        assertEquals(0.0, integralFractionalEquation.functionality(fractionalEquation), 1e-9);
    }

    @Test
    public void functionalityExpIntegral() {
        Integral integralExp = new Integral();
        Exp exp = new Exp(1, 0, 0, 0);
        Exp exp1 = new Exp(1, 0, -1, 0);
        assertEquals(0.0, integralExp.functionality(exp), 1e-9);
        assertEquals(0.619064725359467, integralExp.functionality(exp1), 1e-9);
    }

    @Test
    public void functionalityLinearIntegral() {
        Integral integralLinearFunction = new Integral();
        LinearFunction linearFunction1 = new LinearFunction(0, 1, 0, 1);
        LinearFunction linearFunction = new LinearFunction(1, 2, 0, 1);
        assertEquals(2.4651000000000005, integralLinearFunction.functionality(linearFunction), 1e-9);
        assertEquals(0.9900000000000007, integralLinearFunction.functionality(linearFunction1), 1e-9);
    }

}